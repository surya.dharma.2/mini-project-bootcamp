<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="position-sticky pt-3">
        <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link {{ Request::is('/dashboard') ? 'active' : '' }}" href="/dashboard">
            <span data-feather="arrow-up-left"></span>
            Dashboard
            </a>
        </li>    
        <li class="nav-item">
            <a class="nav-link {{ Request::is('/dashboard/posts*') ? 'active' : '' }}" aria-current="page" href="/dashboard/posts">
            <span data-feather="home"></span>
            Postingan
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ Request::is('dashboard/saran*') ? 'active' : '' }}" href="/dashboard/saran">
            <span data-feather="file-text"></span>
            Saran
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ Request::is('dashboard/rating') ? 'active' : '' }}" href="/dashboard/rating">
            <span data-feather="user"></span>
            Rating
            </a>
        </li>
    </div>
</nav>