<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/front/navbar.css">
    <link rel="stylesheet" href="@yield('link_css')">
    <title>@yield('title')</title>
</head>
<body>
    <div class="navbar">
        <h2>Guna Guide</h2>
        <a href="home">Beranda</a>
        <a href="destination">Destinasi</a>
        <a href="trip">Buat Trip</a>
        <a href="{{route('logout')}}" class="logout"><button></button></a>
        <form action="search">
            <input class="search" name="search" placeholder="Cari..." required>	
            <button type="submit"></button>
        </form>
    </div>
    @yield('content')
    <div class="footer">
        <div class="layanan">
            <h1>LAYANAN</h1>
            <p>
                <a href="/sarandestinasi">Saran Destinasi</a><br/>
                <a href="#">Hubungi Kami</a>
            </p>
        </div>
        <div class="dukungan">
            <h1>DUKUNGAN</h1>
            <p>
                <a href="#">Tentang</a><br/>
                <a href="#">Ketentuan</a><br/>
                <a href="#">Kebijakan Privasi</a>
            </p>
        </div>
        <div class="ikuti">
            <h1>IKUTI KAMI</h1>
            <p>
                <a href="#">Instagram</a><br/>
                <a href="#">Twitter</a><br/>
                <a href="#">Facebook</a>
            </p>
        </div>
    </div>
</body>
</html>