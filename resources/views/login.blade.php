<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/login_style.css">
    <title>Login | Guna Guide</title>
</head>
<body>
    <div id="main">
        @if(session('loginError'))
            <div class="alert alert-danger">
                <b>Opps!</b> {{session('loginError')}}
            </div>
        @endif
        <img src="img/tes2.jpg">
        <div class="login">
            <h1>Masuk</h1>
            <form action="/login" method="POST">
            @csrf
                <input class="email" type="email" name="email" placeholder="Email" autofocus required>
                <input class="password" type="password" name="password" placeholder="Password" required>
                <button class="acc" type="submit">MASUK</button>
            </form>
            <p><a href="register">belum punya akun</a></p>
        </div>
        <div class="quote">
            <h1>“Gaperlu bingung rencanain<br/> liburan kamu. ”</h1>
            <h2>Guna Guide</h2>
        </div>
    </div>
</body>
</html>