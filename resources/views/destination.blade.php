@extends('layout.navbar')
@section('link_css')
    css/destination_style.css
@endsection

@section('title')
    Destinasi | Guna Guide
@endsection

@section('content')
<div class="back1">
</div>
<form action="destination">
    <select name="tipe_destinasi">
        <option value="Penginapan">Penginapan</option>
        <option value="Tempat Wisata">Tempat Wisata</option>
        <option value="Tempat Makan">Tempat Makan</option>
    </select>
    <button type="submit">Submit</button>
</form>
<div class="destination">
    @foreach ($data as $item)
        <div class="list">
            <table>
                <tr>
                    <th rowspan="2"> <img src="{{ asset('storage/'. $item->image) }}"></th>
                    <th colspan=><h1><a href="/destination/{{ $item->id }}">{{ $item->nama_destinasi}}</a></h1></th>
                    <th rowspan="2"><h3>{{ $item->harga}}</h3></th>
                </tr>
                <tr>
                    <td> <p>{{ $item->preview}}</p></td>
                </tr>
                
            </table>
        </div>
    @endforeach
</div>
@endsection