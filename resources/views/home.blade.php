@extends('layout.navbar')
@section('link_css')
    css/home_style.css
@endsection

@section('title')
    Beranda | Guna Guide
@endsection

@section('content')
<div class="img1">
    <img src="img/tes2.jpg">
    <h2>VISIT</h2>
    <h1>BALI</h1>
    <p>
        Guna Guide merupakan website perjalanan wisata di Bali dimana <br/>
        kami akan merekomendasikan seluruh perjalanan kamu berdasarkan <br/>
        budget, durasi dan jumlah orang yang kamu ajak.
     </p>
</div>
<div class="img2">
    <h1>
        Mulai Sekarang ! <br/>
        GRATIS
    </h1>
    <a href="/trip"><button>Buat Trip</button></a>
</div>
<div class="img3">
    <h2>Dengan lebih dari</h2>
    <h1>
        10.000+<br/><br/>
        3.000+<br/><br/>
        1.000+
    </h1>
    <div class="hotel">
        <p>
            Hotel & Resort
        </p>
    </div>
    <div class="resto">
        <p>
            Restoran
        </p>
    </div>
    <div class="wisata">
        <p>
            Tempat wisata
        </p>
    </div>
</div>
<div class="img4">
    <h2>Mudah digunakan</h2>
    <div class="pertama">
        <img src="img/Langkah1.jpeg">
        <h1>
            Tentukan durasi liburan.
        </h1>
    </div>
    <div class="kedua">
        <img src="img/Langkah2.jpg">
        <h1>
            Tentukan jumlah orang yang kamu ajak.
        </h1>
    </div>
    <div class="ketiga">
        <img src="img/langkah3.jpg">
        <h1>
            Tentukan estimasi budget kamu.
        </h1>
    </div>
    <div class="keempat">
        <h1>
            Pilih kategori liburan yang kamu mau.
        </h1>
    </div>
    <div class="kelima">
        <h1>
            Selesai dan rekomendasi perjalanan akan muncul.
        </h1>
    </div>
</div>
<div class="img5">
    <h1>
        Jadi Gaperlu Bingung <br/>
        Rencanain Liburan <br/>
        kamu
    </h1>
</div>
@endsection