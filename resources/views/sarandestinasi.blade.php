@extends('layout.navbar')
@section('link_css')
    css/destination_style.css
@endsection

@section('title')
    Saran Destinasi | Guna Guide
@endsection

@section('content')
<form action="saran" method="POST">
@csrf
    <div class="form-group">
        <label>Tipe Detinasi:</label><br/>
            <select name="tipe_destinasi">
                <option value="Penginapan">Penginapan</option>
                <option value="Tempat Wisata">Tempat Wisata</option>
                <option value="Tempat Makan">Tempat Makan</option>
            </select>
    </div>
    <div class="form-group">
        <label>Nama Destinasi:</label><br/>
        <input type="text" name="nama_destinasi" placeholder="nama" />
    </div>
    <div class="form-group">
        <label>Deskripsi:</label><br/>
        <textarea cols="40" rows="10" name="deskripsi"></textarea>
    </div>
    <div class="form-group">
        <label>Harga:</label><br/>
        <input type="text" name="harga" />
    </div>
    <input type="submit" value="Upload" class="btn btn-primary">
</form>
@endsection