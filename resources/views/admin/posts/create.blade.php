@extends('admin.dashboard')

@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Create Post</h1>
</div>

<div class="col-lg-8">
<form method="POST" action="/dashboard/posts" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
      <label for="tipe_destinasi" class="form-label">Tipe Destinasi</label>
      <select name="tipe_destinasi" class="form-select ">
        <option value="Penginapan">Penginapan</option>
        <option value="Tempat Wisata">Tempat Wisata</option>
        <option value="Tempat Makan">Tempat Makan</option>
      </select>
    </div>
    <div class="mb-3">
      <label for="nama_destinasi" class="form-label">Nama Destinasi</label>
      <input type="text" class="form-control  @error('nama_destinasi') is-invalid @enderror" id="nama_destinasi" name="nama_destinasi" required autofocus value="{{ old('nama_destinasi') }}">
      @error('nama_destinasi')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="slug" class="form-label">Slug</label>
      <input type="text" class="form-control  @error('slug') is-invalid @enderror" id="slug" name="slug" value="{{ old('slug') }}">
      @error('slug')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="image" class="form-label">Image</label>
      <input type="file" name="image" class="form-control" id="image" @error('image') is-invalid @enderror>
      @error('image')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="deskripsi" class="form-label">Deskripsi</label>
      @error('deskripsi')
          <p class="text-danger">{{ $message }}</p>
      @enderror
      <input id="deskripsi" type="hidden" name="deskripsi" value="{{ old('deskripsi') }}">
      <trix-editor input="deskripsi"></trix-editor>
    </div>
    <div class="mb-3">
      <label for="harga" class="form-label">Harga</label>
      <input type="text" class="form-control  @error('harga') is-invalid @enderror" id="harga" name="harga" value="{{ old('harga') }}">
      @error('harga')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Create Post</button>
</form>
<script>
  document.addEventListener('trix-file-accept', function(e) {
    e.preventDefault();
  })
</script>
</div>
@endsection