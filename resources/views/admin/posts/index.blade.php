@extends('admin.dashboard')

@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">My Posts</h1>
</div>

@if (session()->has('success'))
  <div class="alert alert-success col-lg-8" role="alert">
    {{ session('success') }}
  </div>
@endif

<div class="table-responsive col-lg-8">
  <a href="/dashboard/posts/create" class="btn btn-primary mb-3" style="text-decoration: none ">Create New Post</a>
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Gambar</th>
          <th scope="col">Tipe</th>
          <th scope="col">Nama</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($posts as $post)
          <tr>
            <td>{{ $loop->iteration }}</td>
            <td><img src="{{ asset('storage/'. $post->image) }}" width="100px" height="80px"></td>
            <td>{{ $post->tipe_destinasi }}</td>
            <td>{{ $post->nama_destinasi }}</td>
            <td>
                <a href="/dashboard/posts/{{ $post->slug}}" class="badge bg-info" style="text-decoration: none">detail</a>
                <a href="" class="badge bg-warning" style="text-decoration: none">edit</a>
                <form action="/dashboard/posts/{{ $post->slug }}" method="post" class="d-inline">
                  @method('delete')
                  @csrf
                  <button class="badge bg-danger border-0" onclick="return confirm('Are you sure?')">hapus</button>
                </form>
            </td>
          </tr>
          @endforeach
      </tbody>
    </table>
  </div>

@endsection