@extends('admin.dashboard')

@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Saran</h1>
</div>

<div class="table-responsive col-lg-8">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nama</th>
          <th scope="col">Rating</th>
          <th scope="col">Comment</th>
          <th scope="col">Author</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($posts as $post)
          <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $post->nama_destinasi }}</td>
            <td>{{ $post->rating }}</td>
            <td style="text-align: justify">{{ $post->comment }}</td>
            <td>{{ $post->email }}</td>
          </tr>
          @endforeach
      </tbody>
    </table>
  </div>

@endsection