@extends('admin.dashboard')

@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Saran</h1>
</div>

<div class="table-responsive col-lg-8">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Tipe</th>
          <th scope="col">Nama</th>
          <th scope="col">deskripsi</th>
          <th scope="col">author</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($posts as $post)
          <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $post->tipe_destinasi }}</td>
            <td>{{ $post->nama_destinasi }}</td>
            <td style="text-align: justify">{{ $post->deskripsi }}</td>
            <td>{{ $post->email }}</td>
          </tr>
          @endforeach
      </tbody>
    </table>
  </div>

@endsection