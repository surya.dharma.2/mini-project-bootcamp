@extends('layout.navbar')
@section('link_css')
    css/destination_style.css
@endsection

@section('title')
    Buat Trip | Guna Guide
@endsection

@section('content')
        <div class="back1">
        </div>
        <div class="destination">
            @foreach ($penginapan as $satu)
            @foreach ($wisata as $dua)
                <div class="list">
                    <table>
                        <tr>
                            <th rowspan="2"> <img src="{{ asset('storage/'. $satu->image) }}"></th>
                            <th colspan=><h1><a href="/destination/{{ $satu->id }}">{{ $satu->nama_destinasi}}</a></h1></th>
                            <th rowspan="2"><h3>{{ $satu->harga}}</h3></th>
                        </tr>
                        <tr>
                            <td> <p>{!! $satu->deskripsi !!}</p></td>
                        </tr>
                        <tr>
                            <th rowspan="2"> <img src="{{ asset('storage/'. $dua->image) }}"></th>
                            <th colspan=><h1><a href="/destination/{{ $dua->id }}">{{ $dua->nama_destinasi}}</a></h1></th>
                            <th rowspan="2"><h3>{{ $dua->harga}}</h3></th>
                        </tr>
                        <tr>
                            <td> <p>{!! $dua->deskripsi !!}</p></td>
                        </tr>
                    </table>
                </div>
            @endforeach
            @endforeach
        </div>
@endsection
