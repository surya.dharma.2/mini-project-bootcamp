<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/register_style.css">
    <title>Register | Guna Guide</title>
</head>
<body>
    <div id="main">
        @if(session('error'))
            <div class="alert alert-danger">
                <b>Opps!</b> {{session('error')}}
            </div>
        @endif
        <img src="img/tes2.jpg">
        <div class="registration">
            <h1>Buat Akun</h1>
            <form action="/registration" method="POST">
            @csrf
                <input class="email" type="email" name="email" placeholder="Email" required>
                <input class="nama" type="name" name="name" placeholder="Nama" required>
                <input class="telpon" type="telpon" name="telpon" placeholder="Nomor Telepon" required>
                <input class="password" type="password" name="password" placeholder="Password" required>
                <button class="acc" type="submit">Daftar</button>
            </form>
        </div>
        <div class="quote">
            <h1>“Gaperlu bingung rencanain<br/> liburan kamu. ”</h1>
            <h2>Guna Guide</h2>
        </div>
    </div>
</body>
</html>