@extends('layout.navbar')
@section('link_css')
    css/singlepost_style.css
@endsection

@section('title')
    Post | Guna Guide
@endsection

@section('content')
<div class="back1">
</div>
<div class="destination">
    <div class="list">
        <img src="{{ asset('storage/'. $des->image) }}" width="100px" height="100px">
        <h1>{{ $des->nama_destinasi}}</h1>
        <h3>{{ $des->harga}}</h3>
        <p>{!! $des->deskripsi !!}</p>
    </div>
    <form action="rating" method="POST">
    @csrf
        <table>
            <tr>
                <h1>Rating</h1>
            </tr>
            <tr>
                <input type="text" name="nama_destinasi" readonly value="{{ $des->nama_destinasi }}">
                <fieldset class="rating">
                    <input type="radio" id="star5" name="rating" value="1" /><label class = "full" for="star1" ></label>
                    <input type="radio" id="star4" name="rating" value="2" /><label class = "full" for="star2" ></label>
                    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" ></label>
                    <input type="radio" id="star2" name="rating" value="4" /><label class = "full" for="star3" ></label>
                    <input type="radio" id="star1" name="rating" value="5" /><label class = "full" for="star4" ></label>
                </fieldset>
            </tr>
            <tr>
                 <td>Tulis komentar:</td>
                 <td><textarea cols="30" rows="5" name="comment"></textarea></td>
            </tr>
            <tr>
                 <td><input type="submit" value="Upload"></td>
            </tr>
        </table>
    </form>
</div>
@endsection