@extends('layout.navbar')
@section('link_css')
    css/destination_style.css
@endsection

@section('title')
    Pencarian | Guna Guide
@endsection

@section('content')
    <div class="back1">
    </div>
    <div class="destination">
        @foreach ($post as $item)
            <div class="list">
                <table>
                    <tr>
                        <th rowspan="2"> <img src="{{ asset('storage/'. $item->image) }}"></th>
                        <th colspan=><h1>{{ $item->nama_destinasi}}</h1></th>
                        <th rowspan="2"><h3>{{ $item->harga}}</h3></th>
                    </tr>
                    <tr>
                        <td> <p>{!! $item->deskripsi !!}</p></td>
                    </tr>
                </table>
            </div>
        @endforeach
    </div>
@endsection
