<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="row">
		<div class="container">
 
			<h2 class="text-center my-5">Tutorial Laravel #30 : Membuat Upload File Dengan Laravel</h2>
			
			<div class="col-lg-8 mx-auto my-5">	
 
				@if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
 
				<form action="{{ route('upload.proses') }}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
 
					<div class="form-group">
						<label> Gambar</label><br/>
						<input type="file" name="image">
					</div>
 
					<div class="form-group">
						<label>Tipe Detinasi:</label><br/>
                            <select name="tipe_destinasi">
                                <option value="Penginapan">Penginapan</option>
                                <option value="Tempat Wisata">Tempat Wisata</option>
                                <option value="Tempat Makan">Tempat Makan</option>
                            </select>
                    </div>
                    <div class="form-group">
						<label>Nama Destinasi:</label><br/>
                        <input type="text" name="nama_destinasi" placeholder="nama" />
                    </div>
                    <div class="form-group">
						<label>Deskripsi:</label><br/>
                        <textarea cols="40" rows="10" name="deskripsi"></textarea>
                    </div>
                    <div class="form-group">
						<label>Harga:</label><br/>
                        <input type="text" name="harga" />
					</div>
					<input type="submit" value="Upload" class="btn btn-primary">
				</form>
				
				<h4 class="my-5">Data</h4>
 
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Gambar</th>
							<th>Tipe</th>
                            <th>Nama</th>
                            <th>Deskripsi</th>
                            <th>Harga</th>
						</tr>
					</thead>
					<tbody>
						@foreach($gambar as $g)
						<tr>
							<td><img src="{{ url('/img/'.$g->image) }}"></td>
                            <td>{{$g->tipe_destinasi}}</td>
                            <td>{{$g->nama_destinasi}}</td>
                            <td>{{$g->deskripsi}}</td>
                            <td>{{$g->harga}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>