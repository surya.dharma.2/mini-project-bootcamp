@extends('layout.navbar')
@section('link_css')
    css/trip_style.css
@endsection

@section('title')
    Buat Trip | Guna Guide
@endsection

@section('content')
<div class="back1">
</div>
<div class="trip">
    <h1>Tentukan Opsi Liburan Sesuai Keinginanmu</h1>
    <form action="rekomendasi" method="POST">
    @csrf
        <div class="durasi">
            <p>
                <label>Durasi Liburan:</label>
                <input type="number" min="1" max="10" name="durasi" />
                <h3>hari</h3>
            </p>
        </div>
        <div class="jumlah">
            <p>
                <label>Jumlah Orang:</label>
                <input type="number" min="1" max="10" name="jumlah" />
                <h3>orang</h3>
            </p>
        </div>
        <div class="budget">
            <p>
                <label>Budget:</label>
                <input type="text" name="budget">
            </p>
        </div>
        <button type="submit" class="submit">Submit</button>
    </form>
</div>
<div class="destination"></div>
@endsection