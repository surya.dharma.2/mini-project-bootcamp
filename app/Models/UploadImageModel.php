<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UploadImageModel extends Model
{
    use HasFactory;

    protected $table = 'dest';
    protected $primaryKey = 'id';
    protected $fillable = ['image', 'tipe_destinasi', 'nama_destinasi', 'deskripsi', 'harga'];
}
