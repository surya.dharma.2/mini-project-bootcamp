<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Saran extends Model
{
    use HasFactory;

    protected $table = 'saran';
    protected $primaryKey = 'id_saran';
    protected $fillable = ['tipe_destinasi', 'nama_destinasi', 'deskripsi', 'harga', 'email'];
}
