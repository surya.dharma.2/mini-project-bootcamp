<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    use HasFactory;

    protected $table = 'dest';
    protected $primaryKey = 'id';
    protected $fillable = ['image', 'tipe_destinasi', 'nama_destinasi', 'slug', 'preview', 'deskripsi', 'harga'];

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
