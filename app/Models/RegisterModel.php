<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegisterModel extends Model
{
    use HasFactory;

    protected $table = 'login';
    protected $primaryKey = 'email';
    protected $fillable = ['email', 'name', 'telpon', 'password'];
    protected $hidden = ['password', 'conf_password'];
}
