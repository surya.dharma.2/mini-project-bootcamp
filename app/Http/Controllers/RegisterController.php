<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RegisterModel;
use App\Models\User;

class RegisterController extends Controller
{
    /*public function registrasi(Request $request)
    {
        $registrasi = RegisterModel::create([
            'email' => $request->email,
            'nama' => $request->nama,
            'telpon' => $request->telpon,
            'password' => $request->password,
            'conf_password'=>"same:password",
        ]);
        return redirect('/');
    }*/
    public function registration(Request $request)
    {
        $registration= User::create([
            'email' => $request->email,
            'name' => $request->name,
            'telpon' => $request->telpon,
            'password' => bcrypt( $request->password),
        ]);
        return redirect('/');
    }
}