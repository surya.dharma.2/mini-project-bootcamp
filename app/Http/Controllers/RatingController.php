<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RatingController extends Controller
{
    public function rating(Request $request)
    {
        $rating= Rating::create([
            'nama_destinasi' => $request->nama_destinasi,
            'email' => Auth::user()->email,
            'rating' => $request->rating,
            'comment' => $request->comment
        ]);
        return redirect('/');
    }

    public function show()
    {
        $posts = Rating::all();
        return view('admin.rating.index', ['posts' => $posts]);
    }
}
