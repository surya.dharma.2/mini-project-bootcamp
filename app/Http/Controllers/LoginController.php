<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(){
        return view('/login');
    }
    public function autenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        if(Auth::attempt($credentials)){
            $request->session()->regenerate();

            return redirect()->intended('/dashboard');
        }

    return back()->with('loginError', 'Login Failed');
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function currentUser(){
        return Auth::user();
    }
}
