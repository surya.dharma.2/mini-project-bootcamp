<?php

namespace App\Http\Controllers;

use App\Models\Destination;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DashboardPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Destination::all();
        return view('admin.posts.index', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData =  $request->validate([
            'tipe_destinasi' => 'required',
            'nama_destinasi' => 'required',
            'slug' => 'required', 
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:2048', 
            'deskripsi' => 'required', 
            'harga' => 'required'
		]);
 
        // menyimpan data file yang diupload ke variabel $file
        if ($request->file('image')) {
            $validatedData['image'] = $request->file('image')->store('post-image');
        }
        
        $validatedData['preview'] = Str::limit(strip_tags($request->deskripsi), 200, '...');
 
      	        // isi dengan nama folder tempat kemana file diupload
 
		Destination::create($validatedData);
 
		return redirect('/dashboard/posts')->with('success', 'new has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Destination $post)
    {
        return view('admin.posts.show', [
            'post' => $post
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Destination $post)
    {
        Destination::destroy($post->id);
 
		return redirect('/dashboard/posts')->with('success', 'Post has been deleted');
    }
}
