<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UploadImageModel;

class UploadImageController extends Controller
{
    public function upload(){
		$gambar = UploadImageModel::get();
		return view('upload',['gambar' => $gambar]);
	}
	public function destination(){
		$gambar = UploadImageModel::all();
		return view('destination',['gambar' => $gambar]);
	}
 
	public function proses_upload(Request $request){
		$this->validate($request, [
			'image' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'tipe_destinasi' => 'required',
            'nama_destinasi' => 'required', 
            'deskripsi' => 'required', 
            'harga' => 'required'
		]);
 
		// menyimpan data file yang diupload ke variabel $file
		$file = $request->file('image');
 
		$nama_file = time()."_".$file->getClientOriginalName();
 
      	        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'img';
		$file->move($tujuan_upload,$nama_file);
 
		UploadImageModel::create([
			'image' => $nama_file,
            'tipe_destinasi' => $request->tipe_destinasi,
            'nama_destinasi' =>  $request->nama_destinasi, 
            'deskripsi' =>  $request->deskripsi, 
            'harga' =>  $request->harga
		]);
 
		return redirect()->back();
	}
}
