<?php

namespace App\Http\Controllers;

use App\Models\Saran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SaranController extends Controller
{
    public function saran(Request $request)
    {
        $saran= Saran::create([
            'tipe_destinasi' => $request->tipe_destinasi,
            'nama_destinasi' => $request->nama_destinasi,
            'deskripsi' => $request->deskripsi,
            'harga' => $request->harga,
            'email' => Auth::user()->email
        ]);
        return redirect('/');
    }

    public function show()
    {
        $posts = Saran::all();
        return view('admin.saran.index', ['posts' => $posts]);
    }
}
