<?php

namespace App\Http\Controllers;

use App\Models\Destination;
use App\Models\UploadImageModel;
use Illuminate\Http\Request;


class DestinationController extends Controller
{
    public function destination(Request $request){
        if($request->has('tipe_destinasi')){
            $data = Destination::where('tipe_destinasi', $request->tipe_destinasi)->get();
        }
        else{
            $data = Destination::all();
        }
        //return $data;
        return view('destination',['data'=>$data]);
    }
    public function show(Destination $data){
		return view('singlepost', [
            "singlepost" => $data
        ]);
    }
    public function showw($id) {
        $des = Destination::where('id', $id)->firstOrFail();
        return view('singlepost', compact('des'));
    }
    public function search(){

        if(request('search')){
            $post = Destination::where('nama_destinasi', 'like', '%'. request('search') . '%')->get();
        }
        return view('search', ['post'=>$post]);

    }
    public function rekomendasi(Request $request){
        $durasi = $request->durasi;
        $jumlah = $request->jumlah;
        $budget = $request->budget;

        $rekomen = $budget / $durasi / $jumlah;

        $penginapan = Destination::where('tipe_destinasi', 'like', 'penginapan')->where('harga', '<=', $rekomen)->get();
        $wisata = Destination::where('tipe_destinasi', 'like', 'tempat wisata')->where('harga', '<=', $rekomen)->get();
        $makan = Destination::where('tipe_destinasi', 'like', 'tempat makan')->where('harga', '<=', $rekomen)->get();


        return view('resulttrip', ['penginapan'=>$penginapan, 'wisata'=>$wisata, 'makan'=>$makan]);
    }
}
