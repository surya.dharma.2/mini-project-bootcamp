<?php

use App\Http\Controllers\DestinationController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TripController;
use App\Http\Controllers\UploadImageController;
use App\Http\Controllers\DashboardPostController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\SaranController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', function () {
    return view('/home');
});
Route::get('/home', function () {
    return view('/home');
})->name('home');

Route::get('/destination', [DestinationController::class, 'destination'])->name('destination');
Route::get('/destination/{post:slug}', [DestinationController::class, 'showw'])->name('show');

Route::get('/search', [DestinationController::class, 'search'])->name('search');

Route::middleware('guest')->group(function(){
    Route::get('/login', [LoginController::class, 'login'])->name('login');
    Route::post('/login', [LoginController::class, 'autenticate']);

    Route::get('/register', function () {
        return view('/register');
    });
    Route::post('registration', [RegisterController::class, 'registration']);    
 });


Route::group(['middleware' => 'auth'], function () {
    Route::middleware('role:admin')->group( function(){
        Route::get('/dashboard', function () {
            return view('admin/index');
        });
        Route::resource('/dashboard/posts', DashboardPostController::class);

        Route::get('/dashboard/saran', [SaranController::class, 'show']);
        Route::get('/dashboard/rating', [RatingController::class, 'show']);
        //Route::get('/upload', [UploadImageController::class, 'upload'])->name('upload.index');
        //Route::post('/upload/proses', [UploadImageController::class, 'proses_upload'])->name('upload.proses');
        
    });

    Route::middleware('role:user')->group( function(){
        Route::get('/trip', function () {
            return view('/front/trip');
        })->name('trip');
        Route::get('resulttrip', [TripController::class, 'trip'])->name('resulttrip');

        Route::get('/sarandestinasi', function () {
            return view('/sarandestinasi');
        });
        Route::post('/saran', [SaranController::class, 'saran'])->name('saran');
        
        Route::post('/destination/rating', [RatingController::class, 'rating'])->name('rating');

        Route::post('/rekomendasi', [DestinationController::class, 'rekomendasi'])->name('rekomendasi');
    });

    Route::get('/currentUser', [LoginController::class, 'currentUser'])->name('current-user');
    Route::get('logout', [LoginController::class, 'logout'])->name('logout');
});






